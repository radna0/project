<?php




class Validation
{
    private $data;
    private $errors = [];
    private static $fields = ['sku', 'name', 'price', 'types'];

    public function __construct($post_data)
    {
        $this->data = $post_data;
    }

    public function validateForm()
    {
        foreach (self::$fields as $field) {
            if (!array_key_exists($field, $this->data)) {
                trigger_error("$field is not present in data");
            }
        }

        $this->validateSku();
        $this->validateName();
        $this->validatePrice();
        $this->validateTypes();
        $this->validateData();

        return $this->errors;
    }


    private function validateSku()
    {
        $val = trim($this->data['sku']);
        if (empty($val)) {
            $this->addError('sku', 'Please, submit required data');
        } else {
        }
    }

    private function validateName()
    {
        $val = trim($this->data['name']);
        if (empty($val)) {
            $this->addError('name', 'Please, submit required data');
        } else {
        }
    }

    private function validatePrice()
    {
        $val = trim($this->data['price']);
        if (empty($val)) {
            $this->addError('price', 'Please, submit required data');
        } else {
            if (!preg_match("/^[0-9]+(\.[0-9]{2})?$/", $val)) {
                $this->addError('price', 'Please, provide the data of indicated type');
            }
        }
    }

    private function validateTypes()
    {
        $val = trim($this->data['types']);
        if (empty($val)) {
            $this->addError('types', 'Please, submit required data');
        } else {
        }
    }

    private function validateData()
    {
        $types = trim($this->data['types']);


        $data1 = trim($this->data['size']);

        $data2 = trim($this->data['weight']);

        $data3a = trim($this->data['height']);
        $data3b = trim($this->data['width']);
        $data3c = trim($this->data['length']);

        if ($types == 'dvd') {


            if (empty($data1)) {
                $this->addError('size', 'Please, submit required data');
            } else {
                if (!preg_match("/^[0-9]+$/", $data1)) {
                    $this->addError('size', 'Please, provide the data of indicated type');
                }
            }
        } else if ($types == 'book') {


            if (empty($data2)) {
                $this->addError('weight', 'Please, submit required data');
            } else {
                if (!preg_match("/^[0-9]+$/", $data2)) {
                    $this->addError('weight', 'Please, provide the data of indicated type');
                }
            }
        } else if ($types == 'furniture') {


            if (empty($data3a)) {
                $this->addError('height', 'Please, submit required data');
            } else {
                if (!preg_match("/^[0-9]+$/", $data3a)) {
                    $this->addError('height', 'Please, provide the data of indicated type');
                }
            }


            if (empty($data3b)) {
                $this->addError('width', 'Please, submit required data');
            } else {
                if (!preg_match("/^[0-9]+$/", $data3b)) {

                    $this->addError('width', 'Please, provide the data of indicated type');
                }
            }


            if (empty($data3c)) {
                $this->addError('length', 'Please, submit required data');
            } else {
                if (!preg_match("/^[0-9]+$/", $data3c)) {

                    $this->addError('length', 'Please, provide the data of indicated type');
                }
            }
        }
    }

    private function addError($key, $val)
    {
        $this->errors[$key] = $val;
    }
}
