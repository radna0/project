<?php

require('../templates/database.php');

$sql = 'SELECT * FROM final_product_list  ORDER BY types ';

$result = mysqli_query($conn, $sql);

$products = mysqli_fetch_all($result, MYSQLI_ASSOC);






if (isset($_POST['delete_all'])) {

    if (isset($_POST['delete_id'])) {

        $all_id = $_POST['delete_id'];
        $extract_id = implode(',', $all_id);


        $sql = "DELETE FROM final_product_list WHERE id IN($extract_id) ";
        mysqli_query($conn, $sql);
        header('Location: product-listing.php');
    }
}

if (isset($_POST['add'])) {
    header('location: product-adding.php ');
}




mysqli_free_result($result);

mysqli_close($conn);





?>
<!DOCTYPE html>
<html lang="en">


<?php require('../templates/header.php'); ?>

<form action="product-listing.php" method="POST">
    <nav>
        <div class="container">
            <h1 class="name">Product List</h1>
            <div class="right">
                <!-- <a href="product-adding.php" id="add-btn">ADD</a> -->






                <button type="submit" name="add" id="add-btn">ADD</button>

                <button type="submit" name="delete_all" id="delete-product-btn">MASS DELETE</button>



            </div>
            <hr>
        </div>
    </nav>
    <main>
        <div class="container">
            <?php

            foreach ($products as $product) {

                $dimension = array($product['height'], $product['width'], $product['length']);
                $dimension = implode("x", $dimension);


                $typeSet['dvd'] = function () use ($product) {
                    echo "Size:" . htmlspecialchars($product['size']) . "\n"  . 'MB';
                };


                $typeSet['book'] = function () use ($product) {
                    echo "Weight:" . htmlspecialchars($product['weight'])  . "\n"  . 'KG';
                };


                $typeSet['furniture'] = function () use ($dimension) {
                    echo "Dimension:" . htmlspecialchars($dimension);
                };





                $informations[0] =   htmlspecialchars($product['sku']);
                $informations[1] =   htmlspecialchars($product['name']);
                $informations[2] =   htmlspecialchars($product['price'])  . "\n" . '$';



            ?>




                <div class="product">
                    <div class="inputbox">
                        <input class="inputbox delete-checkbox" type="checkbox" name=" delete_id[]" value="<?php echo $product['id']; ?> ">
                    </div>

                    <?php for ($i = 0; $i < count($informations); $i++) {  ?>

                        <h6 class='details'><?php echo ($informations[$i]);
                                        }   ?> </h6>
                        <h6 class='details'><?php $typeSet[$product['types']]()  ?> </h6>





                </div>
            <?php } ?>



        </div>

    </main>

</form>
<?php require('../templates/footer.php'); ?>

</html>