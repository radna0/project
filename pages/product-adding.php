<?php

require('../templates/database.php');
require('../templates/error.php');

$errors = [];

if (empty($_POST['types'])) {
    $_POST['types'] = '';
}




try {

    if (isset($_POST['submit'])) {

        $validation = new Validation($_POST);
        $errors = $validation->validateform();


        if (empty($errors)) {
            $sku = mysqli_real_escape_string($conn, $_POST['sku']);
            $name = mysqli_real_escape_string($conn, $_POST['name']);
            $price = mysqli_real_escape_string($conn, $_POST['price']);
            $TypeSet = mysqli_real_escape_string($conn, $_POST['types']);


            $types['dvd'] = function () use ($conn, $sku, $name, $price, $TypeSet) {
                $size = mysqli_real_escape_string($conn, $_POST['size']);


                return "INSERT INTO 
             final_product_list(sku,name,price,types,size)

              VALUES('$sku','$name','$price','$TypeSet','$size')";
            };


            $types['book'] = function () use ($conn, $sku, $name, $price, $TypeSet) {
                $weight = mysqli_real_escape_string($conn, $_POST['weight']);


                return "INSERT INTO 
            final_product_list(sku,name,price,types,weight)
    
            VALUES('$sku','$name','$price','$TypeSet','$weight')";
            };


            $types['furniture'] = function () use ($conn, $sku, $name, $price, $TypeSet) {
                $height = mysqli_real_escape_string($conn, $_POST['height']);
                $width = mysqli_real_escape_string($conn, $_POST['width']);
                $length = mysqli_real_escape_string($conn, $_POST['length']);


                return "INSERT INTO 
    
            final_product_list(sku,name,price,types,height,width,length)
    
            VALUES('$sku','$name','$price','$TypeSet','$height','$width','$length')";
            };


            $sql = $types[$TypeSet]();

            mysqli_query($conn, $sql);
            header('location: product-listing.php ');
        }
    }
} catch (Exception $e) {
    $errors['sku'] = 'sku exist';
}


if (isset($_POST['cancel'])) {
    header('location: product-listing.php ');
}

?>

<!DOCTYPE html>
<html lang="en">
<?php require('../templates/header.php'); ?>

<form id="product_form" action="product-adding.php" method="POST">
    <nav>
        <div class="container">
            <h1 class="name">Product Add</h1>
            <span class="right">
                <button type="submit" name="submit" id="save-btn">Save</button>
                <button type="submit" name="cancel" id="cancel-btn">Cancel</button>
                <!-- <a href="product-listing.php" id="cancel-btn">Cancel</a></li> -->
            </span>
            <hr>
        </div>
    </nav>
    <main>
        <div class="container">

            <div>
                <label for="sku">SKU</label>
                <input id="sku" type="text" name="sku" value="<?php echo htmlspecialchars($_POST['sku'] ?? ''); ?>">
                <div class="errors">
                    <?php echo $errors['sku'] ?? ''; ?>
                </div>
            </div>

            <div>
                <label for="name">Name</label>
                <input id="name" type="text" name="name" value="<?php echo htmlspecialchars($_POST['name'] ?? ''); ?>">
                <div class="errors">
                    <?php echo $errors['name'] ?? ''; ?>
                </div>
            </div>

            <div>
                <label for="price">Price&#40;&#36;&#41;</label>
                <input id="price" type="text" name='price' value="<?php echo htmlspecialchars($_POST['price'] ?? ''); ?>">
                <div class="errors">
                    <?php echo $errors['price'] ?? ''; ?>
                </div>
            </div>
            <div>
                <label id="productType-display" for="types">Type Switcher</label>
                <select type="select" name="types" id="productType">
                    <optgroup label="Type switcher">
                        <option id="none" value=""></option>


                        <option value="dvd" <?php if ($_POST['types'] == 'dvd') {
                                                echo "selected";
                                            } ?>>DVD</option>

                        <option value="book" <?php if ($_POST['types'] == 'book') {
                                                    echo "selected";
                                                }  ?>>Book</option>

                        <option value="furniture" <?php if ($_POST['types'] == 'furniture') {
                                                        echo "selected";
                                                    }  ?>>Furniture</option>


                    </optgroup>
                </select>
                <div class="errors">
                    <?php echo $errors['types'] ?? ''; ?>
                </div>
            </div>
            <div>


                <div class="data dvd">
                    <label class="others" name="size" for="size">Size (MB)</label>
                    <input id="size" class="information" type="text" name="size" value="<?php echo htmlspecialchars($_POST['size'] ?? ''); ?>">
                    <div class="errors">
                        <?php echo $errors['size'] ?? ''; ?>
                    </div>
                    <h6>Please, provide disc space in MB</h6>
                </div>

                <div class="data book">
                    <label class="others" name="weight" for="weight">Weight (KG)</label>
                    <input id="weight" class="information" type="text" name="weight" value="<?php echo htmlspecialchars($_POST['weight'] ?? ''); ?>">
                    <div class="errors">
                        <?php echo $errors['weight'] ?? ''; ?>
                    </div>
                    <h6>Please, provide weight in KG.</h6>
                </div>

                <div class="data furniture">
                    <div>
                        <label class="others" name="height" for="height">Height (CM)</label>
                        <input id="height" class="information" type="text" name="height" value="<?php echo htmlspecialchars($_POST['height'] ?? ''); ?>">
                        <div class="errors">
                            <?php echo $errors['height'] ?? ''; ?>
                        </div>
                    </div>
                    <div> <label class="others" name="width" for="width">Width (CM)</label>
                        <input id="width" class="information" type="text" name="width" value="<?php echo htmlspecialchars($_POST['width'] ?? ''); ?>">
                        <div class="errors">
                            <?php echo $errors['width'] ?? ''; ?>
                        </div>
                    </div>
                    <div> <label class="others" name="length" for="length">Length (CM)</label>
                        <input id="length" class="information" type="text" name="length" value="<?php echo htmlspecialchars($_POST['length'] ?? ''); ?>">
                        <div class="errors">
                            <?php echo $errors['length'] ?? ''; ?>
                        </div>
                    </div>
                    <h6>Please, provide dimension in CM.</h6>
                </div>


            </div>

</form>
</div>



</main>
<?php require('../templates/footer.php'); ?>

</html>